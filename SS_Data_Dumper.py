import os
import kivy
import smartsheet
import logging
from kivy.lang import Builder
from kivy.app import App
from kivy.uix.screenmanager import ScreenManager, Screen 
from kivy.properties import ObjectProperty
from kivy.uix.boxlayout import BoxLayout 
from kivy.uix.screenmanager import FadeTransition
from kivy.uix.label import Label
from datetime import date
import time

class WelcomeScreen(Screen):
	pass

class DataDumperApp(App):
	def build(self):
		screenmanager = screen_manager = ScreenManager(transition=FadeTransition())
		screen_manager.add_widget(WelcomeScreen(name="welcomescreen"))
		self.icon = 'Icon.png'
		return screen_manager	

ddapp = DataDumperApp()
ddapp.run()